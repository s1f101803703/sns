from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from .models import BoardModel
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.

def signupfunc(request):
  if request.method =='POST':
    username2 = request.POST['username']
    password2 = request.POST['password']
    try:
      User.objects.get(username=username2)
      return render(request, 'signup.html', {'error':'このユーザーは登録されています'})
    except:
      user = User.objects.create_user(username2, '', password2)
      return redirect('login')
  return render(request, 'signup.html', {'some':100})

def loginfunc(request):
  if request.method =='POST':
    username2 = request.POST['username']
    password2 = request.POST['password']
    user = authenticate(request, username=username2, password=password2)
    if user is not None:
      login(request, user)
      return redirect('list')
    else:
      return redirect('login')
  return render(request, 'login.html')

@login_required
def listfunc(request):
  object_list = BoardModel.objects.order_by('-date').all()
  page_obj = paginate_query(request, object_list, 3)
  return render(request, 'list.html', {'object_list':object_list, 'page_obj' : page_obj})

def paginate_query(request, queryset, count):
  paginator = Paginator(queryset, count)
  page = request.GET.get('page')
  try:
    page_obj = paginator.page(page)
  except PageNotAnInteger:
    page_obj = paginator.page(1)
  except EmptyPage:
    page_obj = paginatot.page(paginator.num_pages)
  return page_obj

def logoutfunc(request):
    logout(request)
    return redirect('login')

def detailfunc(request, pk):
  object = BoardModel.objects.get(pk=pk)
  return render(request, 'detail.html', {'object':object})

def goodfunc(request, pk):
  post = BoardModel.objects.get(pk=pk)
  post.good = post.good + 1
  post.save()
  return redirect('list')

def readfunc(request, pk):
  post = BoardModel.objects.get(pk=pk)
  post2 =request.user.get_username()
  if post2 in post.readtext:
    return redirect('list')
  else:
    post.read += 1
    post.readtext = post.readtext + ' ' + post2
    post.save()
    return redirect('list')

class BoardCreate(CreateView):
  template_name = 'create.html'
  model = BoardModel
  fields = ('title', 'content', 'author', 'images')
  success_url = reverse_lazy('list')
